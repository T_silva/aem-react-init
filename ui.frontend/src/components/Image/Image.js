import React, {Component} from 'react';
import {MapTo} from '@adobe/aem-react-editable-components';

require("./Image.css"); 

export const ImageEditConfig = {
    emptyLabel: "Image",

    isEmpty: function(props) {
        return !props || !props.src || props.src.trim().length < 1; 
    }
};

export default class Image extends Component {

    // get image's properties of the component Image
    get content() {
        return <img 
                className="Image-src"
                src={this.props.src}
                alt={this.props.alt}
                tiitle={this.props.title ? this.props.title : this.props.alt}/>;
    }

    render() {
        if ( ImageEditConfig.isEmpty(this.props) ) {
            return null
        }

        return (
            //render componment call content function
            <div className="Image">
                {this.content}
            </div>
        );
    }

}

MapTo("wknd-spa-react/components/image")(Image,ImageEditConfig);