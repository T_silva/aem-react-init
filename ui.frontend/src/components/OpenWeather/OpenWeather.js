import React from 'react';
import { MapTo } from '@adobe/aem-react-editable-components';
import ReactWeather, {useOpenWeather} from 'react-open-weather'

const API_KEY = '9dfd2b904c8a17203e19a9c25dddae46';

const OpenWeatherEditConfig = {
    EmptyLabel: 'Weather',

    isEmpty: function(props) {
        return !props || !props.lat || !props.lon || !props.label;
    }
};

function ReactWeatherWrapper(props) {
    const {data, isLoading, errorMessage} = useOpenWeather({
        key: API_KEY,
        lat: props.lat,
        lon: props.lon,
        lang: 'en',
        unit: 'imperial'
    });

    return (
        <div className="cmp-open-weather">
            <ReactWeather 
                isLoading={isLoading}
                errorMessage={errorMessage}
                data={data}
                lang='en'
                locationLabel={props.label}
                unitsLabel={{temperature: 'C', windSpeed: 'Km/h'}}
                showForecast={false}/>
        </div>
    )
}

export default function OpenWeather(props) {

    if(OpenWeatherEditConfig.isEmpty(props)) {
        return null;
    }

    return ReactWeatherWrapper(props);
}

MapTo('wknd-spa-react/components/open-weather')(OpenWeather, OpenWeatherEditConfig);

